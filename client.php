<?php
// This file is NOT a part of Moodle - http://moodle.org/
//
// This client for Moodle 2 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//

/**
 * REST client for Moodle 3.8
 *
 * @author Daniel Neis Araujo
 */

require_once('./curl.php');

$token = '';
$domainname = '';

$serverurl = $domainname . '/webservice/rest/server.php?moodlewsrestformat=json&wstoken=' . $token . '&wsfunction=';

// Criar usuários
$functionname = 'core_user_create_users';
$users = [
    [
        'createpassword' => true,
        'username' => 'abcxyz',
        'idnumber' => 'xyzabc',
        'firstname' => 'Usuario',
        'lastname' => 'Teste',
        'email' => 'teste@email.com'
    ],
    [
        'createpassword' => true,
        'username' => '11111111111',
        'idnumber' => '11111111111',
        'firstname' => 'Usuario 2',
        'lastname' => 'Teste 2',
        'email' => 'teste2@email.com'
    ],
];
$params = ['users' => $users];

$curl = new curl;
header('Content-Type: text/plain');
$resp1 = $curl->post($serverurl . $functionname, $params);
$resp = json_decode($resp1);
var_dump($resp);

// Obter usuários:
$functionname = 'core_user_get_users_by_field';
$params = ['field' => 'idnumber', 'values' => ['xyzabc']]; // Também pode user "username" ou "email".

$curl = new curl;
header('Content-Type: text/plain');
$resp1 = $curl->post($serverurl . $functionname, $params);
$resp = json_decode($resp1);
var_dump($resp);

// Criar categorias de cursos: 
$functionname = 'core_course_create_categories';
$categories = [
    [
        'name' =>  'Categoria teste 1',
        'parent' => 0,
        'idnumber' => 'Identificador da categoria 1',
    ],
    [
        'name' =>  'Categoria teste 2',
        'parent' => 0,
        'idnumber' => 'Identificador da categoria 2',
    ]
];
$params = ['categories' => $categories];

$curl = new curl;
header('Content-Type: text/plain');
$resp1 = $curl->post($serverurl . $functionname, $params);
$resp = json_decode($resp1);
var_dump($resp);

// Obter Categorias:
$functionname = 'core_course_get_categories';
$criteria = [
    [
        'key' => 'idnumber',
        'value' => 'Identificador da categoria 1',
    ],
];
$params = ['criteria' => $criteria, 'addsubcategories' => 0];

$curl = new curl;
header('Content-Type: text/plain');
$resp1 = $curl->post($serverurl . $functionname, $params);
$resp = json_decode($resp1);
var_dump($resp);

// Criar cursos: 
$functionname = 'core_course_create_courses';
$courses = [
    [
        'fullname' => 'Nome completo do curso 1',
        'shortname' => 'Nome curto (identificador) do curso 1',
        'categoryid' => '1', // ID da categoria do curso
    ],
];
$params = ['courses' => $courses];

$curl = new curl;
header('Content-Type: text/plain');
$resp1 = $curl->post($serverurl . $functionname, $params);
$resp = json_decode($resp1);
var_dump($resp);

// Obter Cursos:
$functionname = 'core_course_get_courses_by_field';
$params = ['field' => 'idnumber', 'value' => 'Nome curto (identificador) do curso 1'];

$curl = new curl;
header('Content-Type: text/plain');
$resp1 = $curl->post($serverurl . $functionname, $params);
$resp = json_decode($resp1);
var_dump($resp);

// Inscrever usuários: 
$functionname = 'enrol_manual_enrol_users';
$enrolments = [
    [
        'userid' => 2,
        'courseid' => 2,
        'roleid' => 5, // 5 é estudante, por padrão.
    ],
];
$params = ['enrolments' => $enrolments];

$curl = new curl;
header('Content-Type: text/plain');
$resp1 = $curl->post($serverurl . $functionname, $params);
$resp = json_decode($resp1);
var_dump($resp);

// Desinscrever usuários: 
$functionname = 'enrol_manual_unenrol_users';
$enrolments = [
    [
        'userid' => 2,
        'courseid' => 2,
        'roleid' => 5, // 5 é estudante, por padrão.
    ],
];
$params = ['enrolments' => $enrolments];

$curl = new curl;
header('Content-Type: text/plain');
$resp1 = $curl->post($serverurl . $functionname, $params);
$resp = json_decode($resp1);
var_dump($resp);

// Atribuição de papéis
$functioname = 'core_role_assign_roles';
$assignments = [
    'roleid' => 00, // ID do papel a ser atribuído
    'userid' => 00, // ID do usuário que receberá o papel
    'contextid' => 00, // ID do contexto onde o usuário receberá o papel (opcional)
    'contextlevel' => 'user', // Nível de contexto: block, course, coursecat, system, user, module
    'instanceid' => 00 // ID da instância conforme nível de contexto.
                       // No caso de user é o id do usuário, no caso de course é o id do curso, etc. (opcional)
];
$params = ['assignments' => $assignments];

$curl = new curl;
header('Content-Type: text/plain');
$resp1 = $curl->post($serverurl . $functionname, $params);
$resp = json_decode($resp1);
var_dump($resp);

// Remover atribuição de papéis
$functioname = 'core_role_unassign_roles';
$unassignments = [
    'roleid' => 00, // ID do papel a ser atribuído
    'userid' => 00, // ID do usuário que terá o papel removido
    'contextid' => 00, // ID do contexto onde o usuário terá o papel removido (opcional)
    'contextlevel' => 'user', // Nível de contexto: block, course, coursecat, system, user, module (opcional)
    'instanceid' => 00 // ID da instância conforme nível de contexto.
                       // No caso de user é o id do usuário, no caso de course é o id do curso, etc. (opcional)
];
$params = ['unassignments' => $unassignments];

$curl = new curl;
header('Content-Type: text/plain');
$resp1 = $curl->post($serverurl . $functionname, $params);
$resp = json_decode($resp1);
var_dump($resp);

// Obter cursos de um usuário:
$functionname = 'enrol_get_users_courses';
$params = [
    'userid' => 2,
    'returnusercount' => false
];

$curl = new curl;
header('Content-Type: text/plain');
$resp1 = $curl->post($serverurl . $functionname, $params);
$resp = json_decode($resp1);
var_dump($resp);

// Obter usuários inscritos em um curso:
$functionname = 'enrol_get_enrolled_users';
$params = [
    'courseid' => 2,
];

$curl = new curl;
header('Content-Type: text/plain');
$resp1 = $curl->post($serverurl . $functionname, $params);
$resp = json_decode($resp1);
var_dump($resp);

// Criar grupos:
$functionname = 'core_group_create_groups';
$groups = [
    [
        'courseid' => 2,
        'name' => 'Nome do grupo',
        'idnumber' => 'Identificador do grupo',
    ],
];
$params = ['groups' => $groups];

$curl = new curl;
header('Content-Type: text/plain');
$resp1 = $curl->post($serverurl . $functionname, $params);
$resp = json_decode($resp1);
var_dump($resp);

// Criar agrupamentos: 
$functionname = 'core_group_create_groupings';
$groupings = [
    [
        'courseid' => 2,
        'name' => 'Nome do grupo',
        'idnumber' => 'Identificador do grupo',
    ],
];
$params = ['groupings' => $groupings];

$curl = new curl;
header('Content-Type: text/plain');
$resp1 = $curl->post($serverurl . $functionname, $params);
$resp = json_decode($resp1);
var_dump($resp);

// Adicionar membros ao grupo: 
$functionname = 'core_group_add_group_members';
$members = [
    [
        'groupid' => 1,
        'userid' => 2
    ],
];
$params = ['members' => $members];

$curl = new curl;
header('Content-Type: text/plain');
$resp1 = $curl->post($serverurl . $functionname, $params);
$resp = json_decode($resp1);
var_dump($resp);

// Adicionar grupo ao agrupamento: 
$functionname = 'core_group_assign_grouping';
$assignments = [
    [
        'groupingid' => 1,
        'groupid' => 1
    ],
];
$params = ['assignments' => $assignments];

$curl = new curl;
header('Content-Type: text/plain');
$resp1 = $curl->post($serverurl . $functionname, $params);
$resp = json_decode($resp1);
var_dump($resp);

// Obter grupos:
$functionname = 'core_group_get_groups';
$groupids = [1, 2, 3];
$params = ['groupids' => $groupids];

$curl = new curl;
header('Content-Type: text/plain');
$resp1 = $curl->post($serverurl . $functionname, $params);
$resp = json_decode($resp1);
var_dump($resp);

// Obter agrupamentos: 
$functionname = 'core_group_get_groupings';
$groupingids = [1, 2, 3];
$params = ['groupingids' => $groupingids, 'returngroups' => 1]; // para retornar também os grupos.

$curl = new curl;
header('Content-Type: text/plain');
$resp1 = $curl->post($serverurl . $functionname, $params);
$resp = json_decode($resp1);
var_dump($resp);

// Excluir usuários
$functionname = 'core_user_delete_users';
$userids = [4, 5, 6, 7]; // pode usar o ID que volta no resultado da core_user_get_users
$params = ['userids' => $userids];

$curl = new curl;
header('Content-Type: text/plain');
$resp1 = $curl->post($serverurl . $functionname, $params);
$resp = json_decode($resp1);
var_dump($resp);

// Excluir categorias de cursos:
$functionname = 'core_course_delete_categories';
$id = 4; // pode usar o ID que volta no resultado da core_course_get_categories
$params = ['categories' => [['id' => $id, 'newparent' => 1, 'recursive' => 0]]];

$curl = new curl;
header('Content-Type: text/plain');
$resp1 = $curl->post($serverurl . $functionname, $params);
$resp = json_decode($resp1);
var_dump($resp);

// Excluir cursos:
$functionname = 'core_course_delete_courses';
$ids = [4, 5, 6, 7]; // pode usar o ID que volta no resultado da core_course_get_courses
$params = ['courseids' => $ids];

$curl = new curl;
header('Content-Type: text/plain');
$resp1 = $curl->post($serverurl . $functionname, $params);
$resp = json_decode($resp1);
var_dump($resp);

// Excluir membros do grupo: 
// TODO
$functionname = 'core_group_delete_group_members';

// Excluir agrupamentos: 
// TODO
$functionname = 'core_group_delete_groupings';

// Excluir grupos: 
// TODO
$functionname = 'core_group_delete_groups';

// Obter itens de notas:
$functionname = 'gradereport_user_get_grade_items';

$params = ['courseid' => $courseid];

$curl = new curl;
header('Content-Type: text/plain');
$resp1 = $curl->post($serverurl . $functionname, $params);
$resp = json_decode($resp1);
var_dump($resp);

// Obter todas as notas de um curso:
$functionname = 'gradereport_user_get_grades_table';

$params = ['courseid' => $courseid];

$curl = new curl;
header('Content-Type: text/plain');
$resp1 = $curl->post($serverurl . $functionname, $params);
$resp = json_decode($resp1);
var_dump($resp);

// TODO
$functionname = 'gradereport_overview_get_course_grades';

// Obter presenças:
// TODO
